# Course Practical Deep Learning in Computer Vision

Week 1 (Ha)

Introduction to the Course: instructors, students, syllabus, policy, expectations etc.
Understand convolutional neural networks (CNNs) 
Recognize dogs and cats
Exercise 1

Week 2 (Tuan) 

CNNs: improve your image classifier (dogs vs cats)
Data augmentation
Hyperparameter tuning
TensorBoard
Exercise 2

Week 3 (Kien)

Object recognition: ResNets from scratch
Transfer learning
Google Colab
Exercise 3

Week 4 (Kien)

Object detection: single shot multiple detector (SSD)
Transfer learning
AWS setup
Exercise 4

Week 5 (Kien)

DarkNet architecture - Yolov3
Generative Adversarial Networks (GANs)
Exercise 5

Week 6 (Quang)

CycleGANs
Data ethics
Style transfer
Exercise 6

Week 7 (Quang)
 
Super resolution
Segmentation with Unets
Exercise 7

Week 8 (Ha/Lino)

Tensorflow serving
Face Detection and Recognition
Face Detection (Dlib)
Face Recognition (FaceNet)
Detecting and recognising faces from a single image
Detecting and recognition faces from your webcam/camera
Exercise 8
